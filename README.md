# label-propagation-2015

Dynamic label propagation in a toy stationary system, reproducing the example in Fig. 1 in original publication by Sokol and Portais DOI:10.1371/journal.pone.0144652.